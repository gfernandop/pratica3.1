/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

/**
 *
 * @author gab
 */
public class loginBean {
    
    public String login = "";
    public String senha = "";
    public String nomePerfil = "";
    public int perfil;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }
    
    public String getNomePerfil(){
        String retorno = "";
        if(this.perfil == 1){
            retorno = "Cliente";
        }
        if(this.perfil == 2){
            retorno = "Gerente";
        }
        if(this.perfil == 3){
            retorno = "Administrador";
        }
   
    return retorno;
    }

    public void setNomePerfil(String nomePerfil) {
        this.nomePerfil = nomePerfil;
    }
        
        
}
